public enum Face {
    Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King;

    public Face next() {
        /**
         * NOTE TO SELF:
         * Figure out how this method works
         */
        int index = (this.ordinal() + 1) % this.values().length;
        return this.values()[index];
    }
}
