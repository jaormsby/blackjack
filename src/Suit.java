public enum Suit {
    Spades, Diamonds, Clubs, Hearts;

    public Suit next() {
        /**
         * NOTE TO SELF:
         * Figure out how this method works
         */
        int index = (this.ordinal() + 1) % this.values().length;
        return this.values()[index];
    }
}
