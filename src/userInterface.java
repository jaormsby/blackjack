import java.util.Scanner;

public class UserInterface {
    public static void main(String[] args) {
        UserInterface ui = new UserInterface();
        ui.initializeGame();
        Scanner myScanner = new Scanner(System.in);
        while(true) {
            System.out.println(ui.getUserInput(myScanner.nextInt()));
        }

        }

    UserInterface() {
        System.out.println("Welcome to Blackjack Trainer!");

    }

    public String initializeGame() {
        /**
         * TODO: implement new/load game functionality
         */

                        return "";
                    }

                public String mainMenu () {
                    Scanner input = new Scanner(System.in);

                    boolean inputNeeded = true;
                    String userInput = "";

                    String[] allowedInputs = {"s", "n", "l", "r", "i", "x"};

                    while (inputNeeded) { //"Enter 'h' to hit, 's' to stand, 'd' to double down, 'x' to split, or 'i' to buy insurance."
                        System.out.println("MAIN MENU: Enter a command. Type 'help' for a list of commands.");
                        userInput = input.next();

                        if (userInput.equals("help")) {
                            System.out.println("s : save game");
                            System.out.println("n : new game");
                            System.out.println("l : load game");
                            System.out.println("r : resume current game");
                            System.out.println("i : statistics");
                            System.out.println("x : exit");

                            userInput = input.next();
                        }

                        for (String s : allowedInputs) {
                        if (userInput.equals(s)) {
                            inputNeeded = false;
                    }
                }

                if (inputNeeded) {
                    System.out.println("Error: command '" + userInput + "' not recognized.");
                }
            }

        return userInput;
    }

    public String gameMenu () {
        Scanner input = new Scanner(System.in);

        boolean inputNeeded = true;
        String userInput = "";

        String[] allowedInputs = {"h", "s", "dd", "spl", "ins", "mm"};

        while (inputNeeded) { //"Enter 'h' to hit, 's' to stand, 'd' to double down, 'x' to split, or 'i' to buy insurance."
            System.out.println("Enter a command. Type 'help' for a list of commands.");
            userInput = input.next();

            if (userInput.equals("help")) {
                System.out.println("h : hit");
                System.out.println("s : stand");
                System.out.println("dd : double down");
                System.out.println("spl : split");
                System.out.println("ins : buy insurance");
                System.out.println("mm : return to main menu");

                userInput = input.next();
            }

            for (String s : allowedInputs) {
                if (userInput.equals(s)) {
                    inputNeeded = false;
                }
            }

            if (inputNeeded) {
                System.out.println("Error: command '" + userInput + "' not recognized.");
            }
        }

        return userInput;
    }

    public String getUserInput(int i) {
        if (i == 0) {
            return initializeGame();
        }
        else if (i == 1) {
            return mainMenu();
        }
        else if (i == 2) {
            return gameMenu();
        }

        return "";
    }

}
