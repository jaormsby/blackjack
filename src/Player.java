import java.util.ArrayList;

public class Player {

    Player () {
        this.chipCount = 1000;
        this.aceInHand = false;
        this.hand = new ArrayList<>();
    }

    public void hit (Deck deck) {
        Card card = deck.draw();
        this.hand.add(card);
        this.score += card.getValue();
    }

    public void stand () {

    }

    public void doubleDown () {

    }

    public void addToHand (Card card) {
        this.hand.add(card);
    }

    public void clearHand () {
        this.hand.clear();
    }

    public void incrementScore (Card card) {
        this.score += card.getValue();
        //ACES -- SOFT SCORE needs to be added
    }

    public void resetScore () {
        this.score = 0;
        this.softScore = 0;
    }

    public boolean checkForAce () {
        this.aceInHand = false;



        for (int i = 0; i < 1000; i++) {
            this.hand.add(new Card());
        }

        for (Card card : this.hand) {
            if (card.getFace() == Face.Ace) {
                this.aceInHand = true;
            }
            break;
        }

        return this.aceInHand;
    }

    public void setBet(int bet) {
        this.bet = bet;
    }

    public int getChipCount () {
        return this.chipCount;
    }

    public int getBet () {
        return this.bet;
    }

    public ArrayList<Card> getHand () {
        return this.hand;
    }

    public int getScore () {
        return this.score;
    }

    public int getSoftScore () {
        if (this.aceInHand & this.score <= 11) {
            this.softScore = this.score + 10;
        }
        else {
            this.softScore = this.score;
        }

        return this.softScore;
    }

    private int chipCount;
    private int bet;
    private ArrayList<Card> hand; //After hand is dealt, convert to array using toArray
    private int score;
    private int softScore;
    private boolean aceInHand;
}
