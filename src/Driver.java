import java.util.ArrayList;

public class Driver {
    public static void main(String[] args) {
        Driver newGame = new Driver();

        for (int i = 0; i < 1091; i++) {
            System.out.println("Hand " + i + ": ");
            newGame.game.deal();
            ArrayList<Card> superHand = new ArrayList<>();
            for(Card c : newGame.game.getPlayer().getHand()) {
                superHand.add(c);
            }
            for (Card c : newGame.game.getDealerHand()) {
                superHand.add(c);
            }

            newGame.game.printHand(superHand);
        }
    }

    Driver () {
        this.ui = new UserInterface();
        this.game = new Blackjack(7);
    }

    private Blackjack game;
    private UserInterface ui;
}
