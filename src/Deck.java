public class Deck {

    Deck() {
        this.deck = new Card[52];

        Face face = Face.Ace;
        Suit suit = Suit.Spades;
        int index = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 13; j++) {
                deck[index] = new Card(face, suit);
                face = face.next();
                index++;
            }
            suit = suit.next();
        }

        shuffle();
    }

    Deck(Card[] deck) {
        if (deck.length == 52) {
            this.deck = new Card[52];
            for (int i = 0; i < this.deck.length; i++) {
                this.deck[i] = deck[i];
            }
        }
        else {
            this.deck = null;
        }
    }

    Deck(int deckCount) {
        this.deckCount = deckCount;
        this.deck = new Card[52*this.deckCount];

        Face face = Face.Ace;
        Suit suit = Suit.Spades;
        int index = 0;
        for (int count = 0; count < this.deckCount; count++) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 13; j++) {
                    this.deck[index] = new Card(face, suit);
                    face = face.next();
                    index++;
                }
                suit = suit.next();
            }
        }

        shuffle();
    }

    public void shuffle() {
        for (int i = 0; i < 1000; i++) {
            int firstCard = (int)(Math.random()*this.deck.length);
            int secondCard = (int)(Math.random()*this.deck.length);

                Card temp = this.deck[firstCard];
                this.deck[firstCard] = this.deck[secondCard];
                this.deck[secondCard] = temp;
        }

        this.position = -1;
        resetCount();
    }

    public Card draw() {
        /**
         * TODO: Allow user to cut deck and choose when deck is reshuffled
         */
        if (this.position < 0.75 * (this.deck.length)) {
            this.runningCount += incrementCount(this.deck[this.position + 1]);
            this.position++;
            return this.deck[this.position];
        }
        else {
            shuffle();
            draw();
        }

        //This will NEVER be called, but cannot be set to null to avoid null pointer exception
        return new Card();
    }

    public int incrementCount(Card card) {
        //Hi-Lo
        int increment = 0;
        switch (card.getFace()) {
            case Two:
            case Three:
            case Four:
            case Five:
            case Six:
                increment = 1;
                break;
            case Seven:
            case Eight:
            case Nine:
                increment = 0;
                break;
            case Ten:
            case Jack:
            case Queen:
            case King:
            case Ace:
                increment = -1;
                break;
            default:
                break;
        }

        this.trueCount = this.runningCount / this.deckCount;
        return increment;
    }

    public void resetCount() {
        this.runningCount = 0;
        this.trueCount = 0;
    }

    public Card[] getDeck() {
        return this.deck;
    }

    public int getDeckCount() {
        return this.deckCount;
    }

    public int getRunningCount() {
        return this.runningCount;
    }

    public int getTrueCount() {
        return this.trueCount;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void printDeck() {
        for (int i = 0; i < this.deck.length; i++) {
            System.out.println(this.deck[i].toString());
        }
    }

    private Card[] deck;
    private int deckCount;
    private int position;
    private int runningCount = 0;
    private int trueCount = 0;
}
