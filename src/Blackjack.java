import java.util.ArrayList;

public class Blackjack {

    Blackjack (int deckCount) {
        this.deck = new Deck(deckCount);
        this.player = new User();
        this.dealerHand = new ArrayList<>();
    }

    public void deal () {
        this.player.clearHand();
        dealerHand.clear();

        this.player.addToHand(this.deck.draw());
        this.player.addToHand(this.deck.draw());

        this.dealerHiddenCard = this.deck.draw();
        this.dealerRevealedCard = this.deck.draw();
        this.dealerHand.add(this.dealerHiddenCard);
        this.dealerHand.add(this.dealerRevealedCard);

        //CHECK FOR DEALER SHOWING ACE/10 --> INSURANCE PROMPT
        //CHECK FOR PLAYER BLACKJACK!

        this.playerTurn = true;
    }

    /**
     * TODO: FIX
     */
    public void hit () {
        this.player.addToHand(this.deck.draw());
        this.playerTurn = !checkForBust();

        this.player.printScore();
        System.out.println("Your score is " + this.player.getScore());

        System.out.println("Your hand: ");
        for (Card c : this.player.getHand()) {
            System.out.print(c.toString() + " ");
            System.out.println();
        }
        System.out.println("Dealer hand: ");
        for (Card c : this.dealerHand) {
            System.out.print(c.toString() + " ");
            System.out.println();
        }
    }

    /**
     * TODO: FIX
     */
    public void stand () {
        this.playerTurn = false;

        while (getDealerScore() < 17) {
            this.dealerHand.add(this.deck.draw());

            System.out.println("Dealer score is " + getDealerScore());

            if (getDealerScore() > 21) {
                System.out.println("Dealer busts!");
            }
        }

        if (this.player.getScore() > getDealerScore()) {
            System.out.println("You won the hand!");
        }
        else if (this.player.getScore() == getDealerScore()) {
            System.out.println("Push!");
        }
        else if (this.player.getScore() < getDealerScore()) {
            System.out.println("Dealer wins the hand!");
        }

        System.out.println("Your hand: ");
        for (Card c : this.player.getHand()) {
            System.out.print(c.toString() + ", ");
            System.out.println();
        }
        System.out.println("Dealer hand: ");
        for (Card c : this.dealerHand) {
            System.out.print(c.toString() + ", ");
            System.out.println();
        }
        System.out.println("Dealer score: " + getDealerScore());
    }

    public void doubleDown () {
        hit();
        stand();
    }

    public void split () {
        //Checks for legality of move
        //Converts player's hand into two hands
    }

    public void buyInsurance (int insuranceAmount) {
        //Buy insurance when true count >= 3
    }

    /**
     * TODO: FIX
     * TODO: Update this method after dealer is changed to be extension of user class
     * Method should take a User argument which can be either the player or the dealer
     */
    public boolean checkForBust () {
        if (this.player.getScore() > 21) {
            System.out.println("You busted! Dealer wins hand.");
            return true;
        }
        else {
            return false;
        }
    }

    public Deck getDeck () {
        return this.deck;
    }

    public User getDealer () {
        return this.dealer;
    }

    public User getPlayer () {
        return this.player;
    }

    public ArrayList<Card> getDealerHand () {
        return this.dealerHand;
    }

    public Card getDealerRevealedCard () {
        return this.dealerRevealedCard;
    }

    public Card getDealerHiddenCard () {
        return this.dealerHiddenCard;
    }

    public boolean getPlayerTurn() {
        return this.playerTurn;
    }

    /**
     * TODO: Reduce clutter in Blackjack class
     *  Make dealer instance of User class (extends User)
     */
    public Boolean checkForDealerAce () {
        this.dealerAce = false;

        for (Card c : this.dealerHand) {
            if (c.getFace() == Face.Ace) {
                this.dealerAce = true;
                break;
            }
        }

        return this.dealerAce;
    }

    public int getDealerScore () {
        this.dealerScore = 0;

        for (Card c : this.dealerHand) {
            this.dealerScore += c.getValue();
        }

        if (this.dealerScore <= 11) {
            if (checkForDealerAce()) {
                this.dealerScore += 10;
            }
        }

        return this.dealerScore;
    }

    public void setDealerScore (int score) {
        this.dealerScore = score;
    }

    private Deck deck;
    private User dealer;
    private User player;

    private ArrayList<Card> dealerHand;
    private Card dealerHiddenCard;
    private Card dealerRevealedCard;
    private boolean dealerAce;
    private int dealerScore;

    private boolean splitLegal;
    private ArrayList<ArrayList<Card>> splits;
    boolean insurancePurchased;

    private boolean playerTurn;

    private Trainer trainer;

    /**
     * TOOD: Move this to User class after dealer has been refactored
     * @param hand
     */
    public void printHand (ArrayList<Card> hand) {
        for (int i = 0; i < 9; i++) {
            String toPrint = "";

            for (Card card : hand) {
                char s;
                String f;
                String[] image = {""};

                switch (card.getSuit()) {
                    case Spades:
                        s = '\u2660';
                        break;
                    case Diamonds:
                        s = '\u2666';
                        break;
                    case Clubs:
                        s = '\u2663';
                        break;
                    case Hearts:
                        s = '\u2764';
                        break;
                    default:
                        s = ' ';
                        break;
                }

                switch (card.getFace()) {
                    case Ace:
                        f = "A";
                        break;
                    case Two:
                        f = "2";
                        break;
                    case Three:
                        f = "3";
                        break;
                    case Four:
                        f = "4";
                        break;
                    case Five:
                        f = "5";
                        break;
                    case Six:
                        f = "6";
                        break;
                    case Seven:
                        f = "7";
                        break;
                    case Eight:
                        f = "8";
                        break;
                    case Nine:
                        f = "9";
                        break;
                    case Ten:
                        f = "10";
                        break;
                    case Jack:
                        f = "J";
                        break;
                    case Queen:
                        f = "Q";
                        break;
                    case King:
                        f = "K";
                        break;
                    default:
                        f = " ";
                        break;
                }

                switch (card.getFace()) {
                    case Ace:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "   #   " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾ \t"
                        };
                        break;
                    case Two:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "    #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "  #    " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾ \t"
                        };
                        break;
                    case Three:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "    #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "    #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                        };
                        break;
                    case Four:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "    #  " + f + "| \t",
                                "|" + s + "    #  " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                        };
                        break;
                    case Five:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "  #    " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "    #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                        };
                        break;
                    case Six:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "  #    " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                        };
                        break;
                    case Seven:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "    #  " + f + "| \t",
                                "|" + s + "   #   " + f + "| \t",
                                "|" + s + "   #   " + f + "| \t",
                                "|" + s + "   #   " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                        };
                        break;
                    case Eight:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                        };
                        break;
                    case Nine:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "    #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                        };
                        break;
                    case Ten:
                        image = new String[]{" ___________ \t",
                                "|" + s + "       " + f + "|\t",
                                "|" + s + " # ### " + f + "|\t",
                                "|" + s + " # # # " + f + "|\t",
                                "|" + s + " # # # " + f + "|\t",
                                "|" + s + " # # # " + f + "|\t",
                                "|" + s + " # ### " + f + "|\t",
                                "|" + s + "       " + f + "|\t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                        };
                        break;
                    case Jack:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "    #  " + f + "| \t",
                                "|" + s + "    #  " + f + "| \t",
                                "|" + s + "    #  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                        };
                        break;
                    case Queen:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "  ###  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  #### " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                        };
                        break;
                    case King:
                        image = new String[]{" __________  \t",
                                "|" + s + "       " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  ##   " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "  # #  " + f + "| \t",
                                "|" + s + "       " + f + "| \t",
                                " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                        };
                        break;
                    default:
                        break;
                }

                if (card.equals(this.dealerHiddenCard)) {
                    image = new String[]{" __________  \t",
                            "|          | \t",
                            "|          | \t",
                            "|          | \t",
                            "|          | \t",
                            "|          | \t",
                            "|          | \t",
                            "|          | \t",
                            " ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾  \t"
                    };
                }

                toPrint += image[i] + " ";
            }

            System.out.println(toPrint);
        }

    }
}
