import java.util.ArrayList;

public class Card {

    Card() {
        this.face = Face.King;
        this.suit = Suit.Clubs;
        this.value = -1;
    }

    Card(Face face, Suit suit) {
        this.face = face;
        this.suit = suit;
        setValue();
    }

    @Override
    public String toString() {
        return String.format("%s of %s", this.face, this.suit);
    }

    public void setFace(Face face) {
        this.face = face;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    public void setValue() {
        switch(this.face) {
            case Ace:
                this.value = 1;
                break;
            case Two:
                this.value = 2;
                break;
            case Three:
                this.value = 3;
                break;
            case Four:
                this.value = 4;
                break;
            case Five:
                this.value = 5;
                break;
            case Six:
                this.value = 6;
                break;
            case Seven:
                this.value = 7;
                break;
            case Eight:
                this.value = 8;
                break;
            case Nine:
                this.value = 9;
                break;
            case Ten:
            case Jack:
            case Queen:
            case King:
                this.value = 10;
                break;
            default:
                this.value = -1;
                break;
        }
    }

    public Face getFace() {
        return this.face;
    }

    public Suit getSuit() {
        return this.suit;
    }

    public int getValue() {
        return this.value;
    }


    private Face face;
    private Suit suit;
    private int value;
}
