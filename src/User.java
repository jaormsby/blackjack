import java.util.ArrayList;

public class User {

    User () {
        this.chipCount = 1000;
        this.aceInHand = false;
        this.hand = new ArrayList<>();
    }

    User (int chipCount) {
        this.chipCount = chipCount;
        this.aceInHand = false;
        this.hand = new ArrayList<>();
    }

    /**
     * TODO: migrate hit, stand, doubleDown methods to Blackjack class
     */
    public void hit (Deck deck) {
        Card card = deck.draw();
        this.hand.add(card);
        this.score += card.getValue();
    }

    public void stand () {

    }

    public void doubleDown (Deck deck) {
        setBet(this.bet * 2);
        hit(deck);
        stand();
    }

    public void addToHand (Card card) {
        this.hand.add(card);
    }

    public void clearHand () {
        this.hand.clear();
    }


    public void setBet(int bet) {
        this.bet = bet;
    }

    public int getChipCount () {
        return this.chipCount;
    }

    public int getBet () {
        return this.bet;
    }

    public ArrayList<Card> getHand () {
        return this.hand;
    }

    public boolean checkForAce () {
        this.aceInHand = false;

        for (Card c : this.hand) {
            if (c.getFace() == Face.Ace) {
                this.aceInHand = true;
                break;
            }
        }

        return this.aceInHand;
    }

    public int getScore () {
        this.score = 0;

        for (Card c : this.hand) {
            this.score += c.getValue();
        }

        if (this.score <= 11) {
            if (checkForAce()) {
                this.score += 10;
            }
        }

        return this.score;
    }

    public String printScore () {
        String toPrint = Integer.toString(getScore());

        if (getScore() < 11 && checkForAce()) {
                toPrint = toPrint + " or " + (getScore() - 10);
        }

        return toPrint;
    }

    private int chipCount;
    private int bet;

    private ArrayList<Card> hand;
    private boolean aceInHand;
    private int score;

}
