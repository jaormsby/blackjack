import java.util.ArrayList;

public class Trainer {
    //Any time method is called in Blackjack, call companion Trainer method to verify decision was correct

    public boolean analyzeDecision (int playerScore, int dealerScore, int trueCount) {
        /**
         * TODO: Long list of correct and incorrect moves
         */
        return false;
    }

    public float getProportionCorrectMoves () {
        return (this.correctMoves / this.totalMoves);
    }

    public float getPercentCorrectMoves () {
        return (100 * getProportionCorrectMoves());
    }

    public int getTotalMoves () {
        return this.totalMoves;
    }

    public int getCorrectMoves () {
        return this.correctMoves;
    }

    public int getIncorrectMoves () {
        return (this.totalMoves - this.correctMoves);
    }

    private int totalMoves;
    private int correctMoves;
}
